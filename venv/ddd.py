import sys, pygame
import numpy as np
from numpy import array
from enum import Enum

pygame.init()


class Player(Enum):
    Player1 = 1
    Player2 = -1

class Board:

    def __init__(self):

        self.rows = 6
        self.cols = 7
        self.grid = np.zeros((6, 7))

    def clear(self):
        self.grid = np.zeros((7,7))

    def print(self):
        print(self.grid)

    def range(self, col_index):
        return col_index >= 0 & col_index <= self.cols

    def putDisc(self,player, col_index):
        if not range(col_index):
            return False

        col = np.array(self.grid[:, col_index])
        col_zeros = np.where(col == 0)
        if len(col_zeros[0]) == 0:
            return  False
        row = col_zeros[0][0]
        print("row {}".format(row))
        self.grid[row, col_index] = player
        return True

    def checkWin(self):

        return Player.Player1



# col = np.array([1,1,1,1,1,1,1,1])
# x = [col  == 0]
#
# b = np.where(col% 2==0)
#
# print(x)
# print(b)
# print(len(b[0]))
b = Board()
b.print()
b.putDisc(1,4)
b.print()
b.putDisc(-1,4)
b.print()
b.putDisc(-1,4)
b.print()
b.putDisc(-1,4)
b.print()
b.putDisc(-1,4)
b.print()
b.putDisc(-1,4)
b.print()
b.putDisc(-1,4)
b.print()

