from board import Board
from board import Discs
import numpy as np
import cv2 as cv


class Connect4Game:
    def __init__(self):
        self.board = Board()
        self.curr_disc = Discs.Orange

    def play(self, col_index):
        row = self.board.put_disc(self.curr_disc, col_index)
        if row == -1:
            return False

        win = self.check_win(row, col_index)
        self.curr_disc = -1 * self.curr_disc
        return False

    def check4(self, list):
        counter = 0
        for i in list:
            if i == self.curr_disc:
                counter += 1
            else:
                counter = 0
            if counter == 4:
                return True
        return False


    def check_win_horizontal(self, row):
        the_row = self.board.grid[:][row]
        return self.check4(the_row)

    def check_win_vertical(self, col):
        the_col = self.board.grid[col][:]
        return self.check4(the_col)

    def check_win_diagonal(self, row, col):
        return False

    def check_win(self, row, col):
        if self.check_win_vertical( col):
            return True
        if self.check_win_horizontal(row):
            return True
        if self.check_win_diagonal(row, col):
            return True
        return False

    def print_board(self):
        self.board.print()


game = Connect4Game()
game.print_board()

w = game.play(1)
game.print_board()
if w:
    print("WIN")

w = game.play(2)
game.print_board()
if w:
    print("WIN")


w = game.play(1)
game.print_board()
if w:
    print("WIN")

w = game.play(2)
game.print_board()
if w:
    print("WIN")

w = game.play(1)
game.print_board()
if w:
    print("WIN")

w = game.play(2)
game.print_board()
if w:
    print("WIN")


w = game.play(1)
game.print_board()
if w:
    print("WIN")


# l = [[1,2,3,4,5,6,7],[10,20,30,40,50,60,70]]
# sub_l = l[0][0:4]

#
