import React from 'react';
import AutocompleteDemo from './components/Autocomplete.js';

function App() {
    return (
        <div className={'row'}>
            <div className={'col-12'}>
                <AutocompleteDemo />
            </div>
        </div>

    );
}

export default App;
