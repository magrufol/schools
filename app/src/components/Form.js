import React from 'react';
import TextField from '@material-ui/core/TextField';
import { makeStyles } from '@material-ui/core/styles';
import fetch from "cross-fetch";
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';


const useStyles = makeStyles((theme) => ({
    root: {
        '& .MuiTextField-root': {
            margin: theme.spacing(1),
            width: '25ch',
        },
        ...theme.typography.button,
        backgroundColor: theme.palette.background.paper,
        padding: theme.spacing(1),
    },
    bullet: {
        display: 'inline-block',
        margin: '0 2px',
        transform: 'scale(0.8)',
    },
    title: {
        fontSize: 14,
    },
    pos: {
        marginBottom: 12,
    },
}));

export default function StateTextFields() {
    const classes = useStyles();
    const [name, setName] = React.useState('');
    const handleChange = (event) => {
        //setName(event.target.value);
    };


    const handleSubmit = async event => {
        event.preventDefault();
        let formData = new FormData(event.target);

        const response = await fetch('/search', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            },
            body: 'keyword=' + formData.get('query')
        });

        const res = await response.text();
        setName(res);

    };


    return (
        <form className={classes.root} noValidate onSubmit={handleSubmit} autoComplete="off">
            <div>
                <TextField id="standard-name" name={'query'} label="Search query"  onChange={handleChange} />
                <Button type="submit" variant="outlined" color="primary" className={classes.button}>
                    Search
                </Button>
            </div>
            <Card className={classes.root}>
                <CardContent>
                    <Typography className={classes.title} color="textSecondary" gutterBottom>
                        {name}
                    </Typography>
                </CardContent>
            </Card>
        </form>
    );
}