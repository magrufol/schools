import flask

from flask import request
from flask import render_template
from flask import jsonify


import json
import ngram
from fast_autocomplete import AutoComplete


class GeoJsonLoader:

  def __init__(self, jsonFile):
      self.f = open(jsonFile, encoding='utf-8')
      self.file = json.load(self.f)

  def getNames(self):
      schools_names = list(self.file.keys())
      return schools_names

jsonLoader = GeoJsonLoader('thai-schools.json')
locations = jsonLoader.getNames()

class  AutoCompleteAlgo:

    def __init__(self, words):
        self.words = words
        self.wordsMap = self.convertListToDictOfDicts(words)

    def convertListToDictOfDicts(self, lst):
        d = {}
        for i in lst:
            d[i] ={}
        return d

    def convertListOfTupToDict(self, tup):
        dct = {}
        for k, p in tup:
            dct.setdefault(k,p)

        return dct

    def fast_ac(self, word, max_cost=3, size=10): # dwg + levinstein

        auto_comp = AutoComplete(words=self.wordsMap)
        auto = auto_comp.search(word, max_cost, size)
        return auto

    def ngram(self, word): # 3gram (N = 3))
        G = ngram.NGram(self.words)
        ret_words = G.search(word)
        return self.convertListOfTupToDict(ret_words)

tmp_locations = {'haifa','tel aviv','tel aiv','tel avv','tl aviv','ramat aviv', 'telmond', 'tel yarok', 'adera', 'tel baruh','ramat hashivi','hulon'}
autocompleteAlgo = AutoCompleteAlgo(tmp_locations)
################ FAST AUTO COMPLETE #########################################
autoWords = autocompleteAlgo.fast_ac('tel ava')
d = autocompleteAlgo.convertListToDictOfDicts([])

# thaiWord = 'พิมานวิทย์'
# thaiWord = thaiWord.encode('utf-8')
# autoWords = autocompleteAlgo.fast_ac(locations,thaiWord)

autoWords = autocompleteAlgo.ngram('tel avev')

app = flask.Flask(__name__)
app.config["DEBUG"] = True
i = 0


@app.route('/', methods=['GET'])
def home():
    global i
    # return render_template('index.html')
    s = "";
    f = open('form.txt', 'r')
    s += f.read()
    f.close()
    return s


@app.route('/search', methods=['POST'])
def search():
    s = request.form
    keyword = s.get('keyword')
    auto_words = autocompleteAlgo.ngram(keyword)
    if keyword in auto_words.keys():
        return "found '"+keyword+"'"
    else:
        res = "did you mean '"+next(iter(auto_words))+"'"
        return res


@app.route('/autocomplete', methods=['POST'])
def autocomplete():
    s = request.form
    keyword = s.get('keyword')
    auto_words = autocompleteAlgo.ngram(keyword)
    return json.dumps(auto_words)


app.run()